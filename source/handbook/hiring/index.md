---
layout: markdown_page
title: "Hiring"
---
* [Hiring Process](#hiring-process)
* [Entering New Hires into TriNet](#trinet-process)

## Hiring Process<a name="hiring-process"></a>

1. Create job description.
    * The CEO needs to authorize any new job positions/searches, and agree on the proposed hiring team.
      * A rough estimate of preferred range of compensation, or some other way of dealing with that inevitable interview question, needs to be determined.
    * The description will go on https://about.gitlab.com/jobs site, and will consist of:
      * Job title
      * Preferred geographic region where candidate should reside
      * Pre-amble about GitLab as a company (alternative: the pre-amble may be placed at the top of the jobs page and omitted from the individual descriptions).
      * Description of role
      * Requirements for the role (can be split into must-have’s and nice-to-have’s)
      * Post-amble stating how to apply, and who the hiring manager is.
1. Define hiring team.
    * Roles to be assigned are: (one person can of course handle multiple roles)
      * Person(s) to do first vetting of candidates, selecting applicants  for interview.
      * Person(s) to have (first round) interviews.
      * Optional: Person(s) to have second round interviews.
      * Person(s) to make final decision to make offer.
      * Person(s) actually making the offer, including terms of offer.
      * Person(s) to handle communications with applicants along the way.
1. Define a hiring timeline.
    * Choose from specific deadlines (e.g. all applicants will be reviewed on date X, hear back by date Y), or
    * Choose rolling application process, wherein the review and interview process  happen as applications come in, or
    * Some combination of the above. So for example, wait for X amount of time to gather enough applications, then review in bulk, set up first interviews, and repeat this process until a suitable applicant is found.
1. Publish the job description.
    * Confirm that the CEO (or person authorized by CEO) has signed off on the description, hiring team, and timeline.
    * Create the job posting within the [Workable](https://gitlab.workable.com/backend) tool. The job will then automatically appear on the [GitLab Jobs](https://about.gitlab.com/jobs) site.
1. Optional: advertise the job description.
    * This can be through “soft” referral, e.g. all GitLab staff post link to jobs site on their LinkedIn profiles.
    * And/Or it can be through job boards.
    * Use the [Workable Clipper](http://resources.workable.com/the-workable-clipper) to help source candidates directly from LinkedIn, and  familiarize yourself with the Workable environment, work flow, features, and support desk.
1. Interview Questions.
    * Hiring team to determine which questions need to be asked, and by whom in the team.
    * Homework assignments may be required for some positions.
1. Communication with Applicants
    * Upon receiving the application and reviewing it for the first time:
      * Applicants should receive confirmation of their application, thanking them for submitting their information. This may be an automated message.
      * If information is missing and the applicant seems sufficiently promising (or not enough information to be able to make that determination), the appropriate person from the hiring team should follow up requesting additional information.
    * Timing
      * Interviews should be set up in accordance with the hiring timeline that was defined previously; and applicants should be notified of this process as much as possible.
      * At time of interview, applicant should be told what the timeline is for a decision, and what the next steps are (if any). An example message would be "We are reviewing applications through the end of next week, and will let you know by the end of two weeks from today whether you've been selected for the next round or not. Please feel free to ping us if you haven't heard anything from us by then."
    * Feedback
      * Currently, the CEO needs to review feedback to candidates who have not been selected.
1. Make a decision, make an offer.
    * The CEO needs to authorize offers.
    * Sign up successful applicant and move to onboarding.
    * Inform other applicants that we selected someone else this time. Applicants remain in the database and may be contacted in the future for other roles.


## Entering New Hires into TriNet<a name="trinet-process"></a>

Employer enters the employee data in the HR Passport with the information below

1. Under the My Staff tab- select new hire/rehire and a drop down menu will appear.

1. HR enters all of the necessary information:

    * Company name autopopulates
    * SS
    * Form of address for hire (Mr. Ms, etc.)
    * First name
    * Last name
    * Middle name or initial
    * Country
    * Address
    * Home phone
    * Home email
    * Gener
    * Ethnicity (you must select something - guess if employee declines to state)
    * Military status

At the bottom of the screen, select next

    * TriNet’s start date
    * Reason - drop down menu with options
    * Employment type - Full time or PT options
    * Select reg/temp bubble
    * Employee Class - drop down between regular and commission
    * Estimated annual wages (does not include anything besides base salary)
    * Benefit class
    * Future benefits class -
    * Standard Hours/week - Part time or Full time
    * Business Title - see org chart
    * Job Code - no need to enter anything here
    * FLSA status- drop down options are exempt, non-exempt, computer prof-non-exempt, computer prof- exempt
    * Supervisor - drop down menu of names
    * Compensation Basis
    * Compensation Rate
    * Departments
    * Work Location - drop down menu
    * Pay Group - only one option
    * Employee ID - not necessary
    * Work email
    * Grouping A/level - not necessary
    * Grouping B/sponsor- not necessary

Select next or save (if you select save, it will hold your information)

    * Vacation/PTO - drop down menu only provides one option- select this
    * Sick- drop down menu only provides one option- select this
    * Personal Time - leave blank
    * Floating Holidays - leave blank
    * Birthdate - mm/dd/yyyy
    * Workers compensation- select unknown and it will default to our principle class code for our industry
Window: Describe employees job duties - simple description

After submission -  you will receive a prompt for final submission, select and submit.

Note: if you save the information to finish at a later date, go to the Work Inbox and select New Hires Not Submitted to continue.

1. The employee receives a welcome email the night before their start date.

1. The employee is prompted to log on, complete tax withholding (W4 data), direct deposit information, section 1 of the I-9, and benefits election (if eligible).

1. The employer logs in to HR Passport and is prompted by way of work inbox item, to complete section 2 of the I-9.